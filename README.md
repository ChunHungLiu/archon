# ARChon

Unofficial work of the ARC Runtime.

Designed to work with https://github.com/vladikoff/chromeos-apk

Please see NOTICE.txt for license information.


## This is hosted on BitBucket because GitHub has a 100mb file limit.